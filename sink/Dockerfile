# Use official Node's alpine image as a base
FROM node:10.6-alpine AS base

# Create an environment variable for our default installation path
ENV INSTALL_PATH=/usr/src/sink \
    SINK_HOST=0.0.0.0 \
    SINK_PORT=3000

# Expose default port
EXPOSE $SINK_PORT

RUN apk --no-cache add \
    bash \
    g++ \
    librdkafka \
    make \
    musl-dev \
    python

# Set INSTALL_PATH as the work directory
WORKDIR $INSTALL_PATH

# Copy files that define dependencies
COPY package.json yarn.lock ./

# Install dependencies
RUN yarn install

# Copy remaining source code
COPY . .

# Run in development mode
CMD [ "yarn", "dev" ];
