const KAFKA_IP = process.env.KAFKA_URI;
// Each table in your module will have a Kafka user_consumer. 
// TOPIC_NAME_USER defines the topic name to be used by the user user_consumer.
const TOPIC_NAME_USER = 'user';

var Kafka = require('node-rdkafka');

// Creating consumer
var user_consumer = new Kafka.KafkaConsumer({
  'metadata.broker.list': KAFKA_IP,
  'group.id': 'node-rdkafka-consumer-user'
});

//logging debug messages, if debug is enabled
user_consumer.on('event.log', function(log) {
  console.log(log);
});

//logging all errors
user_consumer.on('event.error', function(err) {
  console.error('Error from consumer');
  console.error(err);
});

// subscribing to the topics after the consumer is ready
user_consumer.on('ready', function(arg) {
  console.log('consumer ready.' + JSON.stringify(arg));

  // You can subscribe to a list of topics
  user_consumer.subscribe([TOPIC_NAME_USER]);
  //start consuming messages
  user_consumer.consume();
});

// This block is called after the consumer receives a message
user_consumer.on('data', function(m) {
  // Output the actual message contents
  console.log(JSON.stringify(m));
  json = JSON.parse(m.value)
  console.log(json);
});

//disconnects the consumer
user_consumer.on('disconnected', function(arg) {
  console.log('consumer disconnected. ' + JSON.stringify(arg));
});

//starting the consumer
user_consumer.connect();

// If you want to disconnect automatically, uncomment this block
//stopping this example after 30s
// setTimeout(function() {
//   user_consumer.disconnect();
// }, 30000);