# Module Template

This project is a template for Hackanizer modules' projects.

Follow these steps to properly configure your repository:

1. change `module/.env` file with your project's info

2. install prisma via NPM
  `npm install -g prisma@1.7.0`

3. after you change your `api/prisma/datamodel.graphql` you have to run `prisma deploy` inside the folder `api/prisma`


# Kafka Setup

## glcloud

1. ```gcloud init ```

2. ```gcloud container clusters get-credentials hacknizer --zone southamerica-east1-a```

## helm

1. ```helm init```

2. ```helm install --name kafka  incubator/kafka```

# Connect to kafka

## Telepresence

```telepresence --docker-run -i -t --rm  -v "$(pwd)/sink:/usr/src/sink" -v  "/usr/src/sink/node_modules"  hacknizer/module_sink``` 

Substitute hacknizer/module_sink for whatever image you want to run connected to kafka

## Expose container port 

```kubectl port-forward deployments/telepresence-1532103851-074687-12145 8080```

You can get the telepresence-1532103851-074687-12145 from 

```kubectl get deployments```

