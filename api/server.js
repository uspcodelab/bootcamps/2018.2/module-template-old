// Koa libraries
import Koa from "koa";
import KoaRouter from "koa-router";
import KoaBodyParser from "koa-bodyparser";

// Apollo Server for Koa
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";

// Prisma
import { Prisma } from "prisma-binding";

// GraphQL Playground Middleware for Koa
const koaPlayground = require("graphql-playground-middleware-koa").default;

// Schema and Resolvers
import { makeExecutableSchema } from "graphql-tools";
import Resolvers from "./graphql/resolvers";
import { importSchema } from "graphql-import";
const typeDefs = importSchema("./graphql/schema.graphql");

const PORT = process.env.API_PORT || 8080;
const NODE_ENV = process.env.NODE_ENV || "development";
const KAFKA_URI = process.env.KAFKA_URI;

const graphQLServer = new Koa();
const router = new KoaRouter();
const bodyParser = new KoaBodyParser();

// Validate GraphQL API Schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers: Resolvers,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  }
});

// Use bodyparser middleware
graphQLServer.use(bodyParser);

// Define GraphQL endpoints
router.post(
  "/graphql",
  graphqlKoa({
    schema,
    context: () => ({
      prisma: new Prisma({
        typeDefs: "graphql/generated/prisma.graphql",
        endpoint: "http://prisma:4466"
      })
    })
  })
);
router.get(
  "/graphql",
  graphqlKoa({
    schema,
    context: () => ({
      prisma: new Prisma({
        typeDefs: "graphql/generated/prisma.graphql",
        endpoint: "http://prisma:4466"
      })
    })
  })
);

// Define endpoint for GraphQL Playground
router.all(
  "/playground",
  koaPlayground({
    endpoint: "/graphql"
  })
);

// Define GraphiQL endpoints
if (NODE_ENV !== "production") {
  router.get("/graphiql", graphiqlKoa({ endpointURL: "/graphql" }));
}

// Use router middleware
graphQLServer.use(router.routes());
graphQLServer.use(router.allowedMethods());

// Each table in your module will have a Kafka producer. 
// TOPIC_NAME_USER defines the topic name to be used by the user producer.
const TOPIC_NAME_USER = 'user';

var Kafka = require('node-rdkafka');

// Creating producer
const userProducer = new Kafka.Producer({
  'metadata.broker.list': KAFKA_URI,
  'dr_cb': true  //delivery report callback
});
var user_producer_is_ready = false;

//logging debug messages, if debug is enabled
userProducer.on('event.log', function(log) {
  console.log(log);
});

//logging all errors
userProducer.on('event.error', function(err) {
  console.error('Error from producer');
  console.error(err);
});

//logs the delivery reports
userProducer.on('delivery-report', function(err, report) {
  console.log('delivery-report: ' + JSON.stringify(report));
});

//polls kafka to check if the messages are being delivered
userProducer.on('ready', function(arg) {
  // user_producer_is_ready = true;
  console.log('producer ready.' + JSON.stringify(arg));
  //need to keep polling for a while to ensure the delivery reports are received
  var pollLoop = setInterval(function() {
    userProducer.poll();
  }, 1000);
});

//disconnects the producer
userProducer.on('disconnected', function(arg) {
  console.log('producer disconnected. ' + JSON.stringify(arg));
});

//starting the producer
userProducer.connect();

export default userProducer;

graphQLServer.listen(PORT, () => {
  console.log(
    `GraphQL Playground is now running on http://localhost:${PORT}/playground`
  );
});
