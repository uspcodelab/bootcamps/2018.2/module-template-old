import userProducer from "../server";
const fetch = require("node-fetch");

const FortuneCookie = {
  getOne() {
    return fetch("http://fortunecookieapi.herokuapp.com/v1/cookie")
      .then(res => res.json())
      .then(res => {
        return res[0].fortune.message;
      });
  }
};

const resolvers = {
  Query: {
    getFortuneCookie() {
      return FortuneCookie.getOne();
    },
    posts: (_, args, context, info) => {
      return context.prisma.query.posts(
        {
          where: {
            OR: [
              { title_contains: args.searchString },
              { content_contains: args.searchString }
            ]
          }
        },
        info
      );
    },
    person: (_, args, context, info) => {
      return context.prisma.query.person(
        {
          where: {
            id: args.id
          }
        },
        info
      );
    }
  },
  Mutation: {
    createUser: (_, args, context, info) => {
      // Send new user information to Kafka
      const x = userProducer.produce('user', -1, new Buffer(JSON.stringify(args)))
      console.log(x);
      return null;
    },
    createDraft: (_, args, context, info) => {
      return context.prisma.mutation.createPost(
        {
          data: {
            title: args.title,
            content: args.title,
            author: {
              connect: {
                id: args.authorId
              }
            }
          }
        },
        info
      );
    },
    publish: (_, args, context, info) => {
      return context.prisma.mutation.updatePost(
        {
          where: {
            id: args.id
          },
          data: {
            published: true
          }
        },
        info
      );
    },
    deletePost: (_, args, context, info) => {
      return context.prisma.mutation.deletePost(
        {
          where: {
            id: args.id
          }
        },
        info
      );
    },
    signup: (_, args, context, info) => {
      return context.prisma.mutation.createPerson(
        {
          data: {
            name: args.name
          }
        },
        info
      );
    }
  }
};

export default resolvers;
